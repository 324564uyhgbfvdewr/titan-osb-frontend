export default (state, index, stage, data = []) => {
  state.steps[index].active = stage === 'active';
  state.steps[index].complete = stage === 'complete';
  state.steps[index].summary = data;
};
