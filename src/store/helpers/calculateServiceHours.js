export default (services, selected) => selected.reduce((acc, serviceName) => {
  const selectedService = services.find(service => service.name === serviceName);
  // eslint-disable-next-line
  acc += selectedService.time;
  return acc;
}, 0);
