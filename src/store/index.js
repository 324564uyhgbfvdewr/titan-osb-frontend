import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import vehicles from './modules/vehicles';
import services from './modules/services';
import workshops from './modules/workshops';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    vehicles,
    services,
    workshops,
    user,
  },
});
