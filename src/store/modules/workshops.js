import axios from '@/api';
import dateFuture from '../../functions/dateFuture';
import dateFormatted from '../../functions/dateFormatted';
import calculateServiceHours from '../helpers/calculateServiceHours';

const defaultState = {
  workshopID: '',
  serviceLeadTime: 60,
  availableWorkshops: [],
  disabledWorkshops: [],
  referenceNumber: '',
};

// getters
const getters = {};

// actions
const actions = {
  getWorkshops: ({
    commit,
    state,
    rootState,
  }) => new Promise((resolve, reject) => {
    const { serviceOptions, selectedServices } = rootState.services;
    const selectedHours = calculateServiceHours(serviceOptions, selectedServices);
    const dateTo = dateFormatted('yyyy-MM-dd', dateFuture(state.serviceLeadTime));
    commit('app/setLoadingState', true, { root: true });
    return axios.get(`Workshops/${state.workshopID}/Capacity?dateTo=${dateTo}`)
      .then((res) => {
        const workshops = res.data.reduce((acc, workshop) => {
          if (workshop.AvailableHours <= selectedHours) acc.disabledWorkshops.push(workshop);
          else acc.availableWorkshops.push(workshop);
          return acc;
        }, {
          availableWorkshops: [],
          disabledWorkshops: [],
        });
        commit('updateAvailableWorkshops', workshops.availableWorkshops);
        commit('updateDisabledWorkshops', workshops.disabledWorkshops);
        resolve();
      })
      .catch(err => reject(err))
      .then(() => {
        commit('app/setLoadingState', false, { root: true });
      });
  }),
  postRepairOrder: ({
    commit,
    state,
    rootState,
  }) => new Promise((resolve, reject) => {
    commit('app/setLoadingState', true, { root: true });
    const { serviceDate, selectedServices, serviceComments } = rootState.services;
    const { profile } = rootState.user;
    const { allVehicles } = rootState.vehicles;

    let CustomerComment = Object.keys(profile).reduce((acc, curr) => `${acc}${profile[curr].title}: ${profile[curr].value}\n`, '');
    CustomerComment += `\nSelected Services: ${selectedServices.join(', ')}\n`;
    CustomerComment += `\nCustomer Comment: ${serviceComments || 'N/A'}`;

    const postData = {
      VehicleId: String(allVehicles[0].Id),
      OwnerProfileId: allVehicles[0].OwnerProfileId,
      Workshop: state.workshopID,
      Odometer: 0,
      CustomerComment,
      ServiceReminder: 'Test',
      ContactNo: '',
      ServiceAdvisor: 'OSB',
      BookingDate: dateFormatted('YYYY-MM-dd', new Date(serviceDate)),
      TimeIn: serviceDate,
      TimeOut: serviceDate,
      DateOut: serviceDate,
      Jobs: [
        {
          OperationCode: '',
          OperationDescription: 'testing 1',
          StandardHour: 0.5,
          LabourAmt: 85.01,
          PartAmt: 0,
          SundryAmt: 0,
          TotalAmt: 85.01,
        },
      ],
    };

    return axios.post('RepairOrders', postData)
      .then((res) => {
        commit('app/completeBooking', null, { root: true });
        commit('app/setLoadingState', false, { root: true });
        commit('workshops/setReferenceNumber', res.data, { root: true });
        return resolve();
      })
      .catch(() => {
        commit('app/setLoadingState', false, { root: true });
        return reject();
      });
  }),
};

// mutations
const mutations = {
  setWorkshopID(state, ID) {
    state.workshopID = ID;
  },
  setReferenceNumber(state, ref) {
    state.referenceNumber = ref;
  },
  setServiceLeadTime(state, days) {
    state.serviceLeadTime = days;
  },
  updateAvailableWorkshops(state, arr) {
    state.availableWorkshops = arr;
  },
  updateDisabledWorkshops(state, arr) {
    state.disabledWorkshops = arr;
  },
};

export default {
  namespaced: true,
  state: defaultState,
  getters,
  actions,
  mutations,
};
