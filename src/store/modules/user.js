const defaultState = {
  profile: {
    Title: {
      title: 'Title',
      validation: 'alpha',
      value: '',
    },
    FirstName: {
      title: 'First Name',
      validation: 'alpha',
      value: '',
    },
    LastName: {
      title: 'Last Name',
      validation: 'alpha',
      value: '',
    },
    Email: {
      title: 'Email',
      validation: 'email',
      value: '',
    },
    Telephone: {
      title: 'Telephone',
      validation: 'numeric',
      value: '',
    },
    HouseNumber: {
      title: 'House Number/Name',
      validation: 'alphanumeric',
      value: '',
    },
    Street: {
      title: 'Street',
      validation: 'alphanumeric',
      value: '',
    },
    Town: {
      title: 'Town',
      validation: 'alpha',
      value: '',
    },
    Postcode: {
      title: 'Postcode',
      validation: 'alphanumeric',
      value: '',
    },
  },
  regex: {
    alphanumeric: new RegExp(/[a-zA-Z0-9 ]+/),
    alpha: new RegExp(/[a-zA-Z ]+/),
    numeric: new RegExp(/\d+/),
    email: new RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/),
  },
};

// getters
const getters = {
  profileErrors(state) {
    return Object.keys(state.profile).reduce((acc, curr) => {
      const field = state.profile[curr];
      const re = state.regex[field.validation];
      if (!re.test(field.value)) acc.push(field.title);
      return acc;
    }, []);
  },
};

// actions
const actions = {};

// mutations
const mutations = {
  updateProfile(state, data) {
    Object.keys(data).forEach((prop) => {
      state.profile[prop].value = data[prop];
    });
  },
};

export default {
  namespaced: true,
  state: defaultState,
  getters,
  actions,
  mutations,
};
