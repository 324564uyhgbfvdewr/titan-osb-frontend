const defaultState = {
  serviceOptions: [
    {
      name: 'Service',
      icon: 'ServiceIcon',
      time: 4,
      description: [],
    },
    {
      name: 'Oil & Filter Change',
      icon: 'OilFilterIcon',
      time: 1,
      description: [],
    },
    {
      name: 'Brake Fluid Change',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Front Brake Pads and Discs',
      icon: 'BrakePadIcon',
      time: 1.5,
      description: [],
    },
    {
      name: 'Rear Brake Pads and Discs',
      icon: 'BrakePadIcon',
      time: 1.5,
      description: [],
    },
    {
      name: 'Front Brake Pads',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Rear Brake Pads/Shoes',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Replace Front Wiper Blades (Pair)',
      icon: 'WipersPairIcon',
      time: 0.2,
      description: [],
    },
    {
      name: 'Rear Windscreen Wiper',
      icon: 'WiperIcon',
      time: 0.2,
      description: [],
    },
  ],
  selectedServices: [],
  serviceComments: '',
  serviceDate: '',
};

// getters
const getters = {};

// actions
const actions = {};

// mutations
const mutations = {
  toggleService(state, stepName) {
    const index = state.selectedServices.indexOf(stepName);
    if (index === -1) state.selectedServices.push(stepName);
    else state.selectedServices.splice(index, 1);
  },
  setServiceDate(state, dateObj) {
    dateObj.setUTCHours(9, 0, 0, 0);
    state.serviceDate = dateObj.toISOString().split('.')[0];
  },
  updateServiceComment(state, text) {
    state.serviceComments = text;
  },
};

export default {
  namespaced: true,
  state: defaultState,
  getters,
  actions,
  mutations,
};
