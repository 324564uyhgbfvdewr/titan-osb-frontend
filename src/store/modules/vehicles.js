import axios from '@/api';
import querystring from '@/functions/querystring';

const defaultState = {
  allVehicles: [],
};

// actions
const actions = {
  getVehicles: ({
    commit,
    state,
  }, payload) => new Promise((resolve, reject) => {
    commit('app/setLoadingState', true, { root: true });
    const string = querystring(payload);
    return axios.get(`Vehicles?${string}`)
      .then((res) => {
        if (res.data.vehicles.length) {
          axios.defaults.headers.common.Authorization = `Bearer ${res.data.token}`;
          state.allVehicles = res.data.vehicles;
        }
        resolve(state.allVehicles);
      })
      .catch(err => reject(err))
      .then(() => {
        commit('app/setLoadingState', false, { root: true });
      });
  }),
};

// mutations
const mutations = {
  clearVehicles: (state) => {
    state.allVehicles = [];
  },
};

// getters
const getters = {};

export default {
  namespaced: true,
  state: defaultState,
  getters,
  actions,
  mutations,
};
