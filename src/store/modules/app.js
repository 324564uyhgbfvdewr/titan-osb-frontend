import setStepState from '../helpers/setStepStage';

const defaultState = {
  isLoading: false,
  isComplete: false,
  steps: [
    {
      name: 'VehicleSearch',
      title: 'Find my vehicle.',
      description: 'Search for your vehicle by  Registration Number or VIN Number.',
      component: 'VehicleContainer',
      active: true,
      complete: false,
      summary: [],
    },
    {
      name: 'ServiceSelect',
      title: 'Service Selection.',
      description: 'Please select the Service Items your vehicle requires from the list below.',
      component: 'ServiceContainer',
      active: false,
      complete: false,
      summary: [],
    },
    {
      name: 'OptionsSelect',
      title: 'Date Selection.',
      description: 'Please book a date for your service.',
      component: 'OptionsContainer',
      active: false,
      complete: false,
      summary: [],
    },
    {
      name: 'PersonalDetails',
      title: 'Your Details.',
      description: 'Please enter your details below.',
      component: 'DetailsContainer',
      active: false,
      complete: false,
      summary: [],
    },
    {
      name: 'Confirmation',
      title: 'Confirmation.',
      description: 'Please review your service booking details below',
      component: 'ConfirmationContainer',
      active: false,
      complete: false,
      summary: [],
    },
  ],
};

// getters
const getters = {
  currentStep(state) {
    return state.steps.find(step => step.active);
  },
};

// actions
const actions = {};

// mutations
const mutations = {
  completeStep(state, { stepName, data }) {
    const stepIndex = state.steps.findIndex(step => step.name === stepName);
    setStepState(state, stepIndex, 'complete', data);

    if ((stepIndex + 1) === state.steps.length) return;
    setStepState(state, stepIndex + 1, 'active');
  },
  revertStep(state, stepName) {
    const stepIndex = state.steps.findIndex(step => step.name === stepName);

    state.steps.forEach((step, index) => {
      if (index < stepIndex) return;
      setStepState(state, index, index === stepIndex ? 'active' : '');
    });
  },
  previousStep(state, stepName) {
    const stepIndex = state.steps.findIndex(step => step.name === stepName);
    setStepState(state, stepIndex, '');
    setStepState(state, stepIndex - 1, 'active');
  },
  setLoadingState(state, bool) {
    state.isLoading = bool;
  },
  completeBooking(state) {
    state.isComplete = true;
  },
};

export default {
  namespaced: true,
  state: defaultState,
  getters,
  actions,
  mutations,
};
