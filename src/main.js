import 'es6-promise/auto';
import 'babel-polyfill';

import Vue from 'vue';

import 'document-register-element/build/document-register-element';
import vueCustomElement from 'vue-custom-element';

import App from './App';
import store from './store';

Vue.config.productionTip = false;

Vue.use(vueCustomElement);

App.store = store;
Vue.customElement('titan-dms-service-widget', App);
