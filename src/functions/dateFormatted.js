const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const days = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const getDay = day => days[day];
const getMonth = month => months[month];

const pad = n => (n < 10 ? `0${n}` : n);

const nth = (d) => {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1: return 'st';
    case 2: return 'nd';
    case 3: return 'rd';
    default: return 'th';
  }
};

export default (format, date = new Date()) => {
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const dayOfWeek = date.getDay();

  switch (format) {
    case 'dd/MM/yyyy':
      return `${pad(day)}/${pad(month + 1)}/${year}`;
    case 'D dsu of MMM':
      return `${getDay(dayOfWeek)} ${day}${nth(day)} of ${getMonth(month)}`;
    default:
      return `${year}-${pad(month + 1)}-${pad(day)}`;
  }
};
