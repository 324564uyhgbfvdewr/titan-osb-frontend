export default (daysInFuture, date = new Date()) => {
  const dateClone = new Date(date.getTime());
  const newDate = dateClone.setDate(dateClone.getDate() + daysInFuture);
  return new Date(newDate);
};
