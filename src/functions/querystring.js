export default queryObject => Object.entries(queryObject)
  .filter(([, value]) => !!value)
  .map(([key, value]) => `${key}=${value}`)
  .join('&');
