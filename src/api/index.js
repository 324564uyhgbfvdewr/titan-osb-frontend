import axios from 'axios';

export default axios.create({
  baseURL: `${process.env.API_URL}${process.env.NODE_ENV}`,
  headers: {
    'Content-Type': 'application/json',
  },
});
