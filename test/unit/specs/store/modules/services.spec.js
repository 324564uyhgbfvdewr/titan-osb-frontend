import services from '@/store/modules/services';
import { cloneDeep } from 'lodash';

let stateClone;
const state = {
  serviceOptions: [
    {
      name: 'Service',
      icon: 'ServiceIcon',
      time: 4,
      description: [],
    },
    {
      name: 'Oil & Filter Change',
      icon: 'OilFilterIcon',
      time: 1,
      description: [],
    },
    {
      name: 'Brake Fluid Change',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Front Brake Pads and Discs',
      icon: 'BrakePadIcon',
      time: 1.5,
      description: [],
    },
    {
      name: 'Rear Brake Pads and Discs',
      icon: 'BrakePadIcon',
      time: 1.5,
      description: [],
    },
    {
      name: 'Front Brake Pads',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Rear Brake Pads/Shoes',
      icon: 'BrakePadIcon',
      time: 0.5,
      description: [],
    },
    {
      name: 'Replace Front Wiper Blades (Pair)',
      icon: 'WipersPairIcon',
      time: 0.2,
      description: [],
    },
    {
      name: 'Rear Windscreen Wiper',
      icon: 'WiperIcon',
      time: 0.2,
      description: [],
    },
  ],
  selectedServices: [],
  serviceComments: '',
  serviceDate: '',
  servicePeriod: '',
};

beforeEach(() => {
  stateClone = cloneDeep(state);
});

describe('Store - Services [mutations]', () => {
  it('toggleService()', () => {
    const options = [
      'Rear Windscreen Wiper',
      'Rear Brake Pads/Shoes',
      'Front Brake Pads',
    ];

    options.forEach((option, index) => {
      services.mutations.toggleService(stateClone, option);
      expect(stateClone.selectedServices.length).toEqual(index + 1);
    });

    options.forEach((option) => {
      services.mutations.toggleService(stateClone, option);
    });
    expect(stateClone.selectedServices.length).toEqual(0);
  });

  it('setServiceDate()', () => {
    const dateRegex = new RegExp(/\d{4}-\d{2}-\d{2}T09:00:00/);
    new Array(10).fill(null).forEach(() => {
      services.mutations.setServiceDate(stateClone, new Date());
      expect(stateClone.serviceDate).toMatch(dateRegex);
    });
  });

  it('updateServiceComment()', () => {
    const words = [
      'foo',
      'bar',
      'baz',
    ];

    words.forEach((word) => {
      services.mutations.updateServiceComment(stateClone, word);
      expect(stateClone.serviceComments).toEqual(word);
    });
  });
});
