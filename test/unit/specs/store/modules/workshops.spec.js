import axios from '@/api';
import MockAdapter from 'axios-mock-adapter';
import workshops from '@/store/modules/workshops';
import { cloneDeep } from 'lodash';
import flushPromises from '../../../mocks/flushPromises';

jest.mock('@/store/helpers/calculateServiceHours', () => jest.fn().mockReturnValue(10));

let stateClone;
let commit;
const mockAxios = new MockAdapter(axios);

const dateObj = new Date(2018, 7, 16);

const state = {
  workshopID: '7',
  serviceLeadTime: 60,
  availableWorkshops: [],
  referenceNumber: '',
};

const rootState = {
  user: {
    profile: {
      Foo: {
        title: 'foo',
        value: 'bar',
      },
    },
  },
  services: {
    serviceDate: dateObj,
    serviceOptions: [],
    selectedServices: ['foo'],
  },
  vehicles: {
    allVehicles: [
      {
        Id: 'foo',
        OwnerProfileId: 'bar',
      },
    ],
  },
};


describe('Store - Workshops [Mutations]', () => {
  beforeEach(() => {
    stateClone = cloneDeep(state);
    commit = jest.fn();
  });

  it('setWorkshopID()', () => {
    workshops.mutations.setWorkshopID(stateClone, 'test');
    expect(stateClone.workshopID).toEqual('test');
  });
  it('setReferenceNumber()', () => {
    workshops.mutations.setReferenceNumber(stateClone, 123);
    expect(stateClone.referenceNumber).toEqual(123);
  });
  it('setServiceLeadTime()', () => {
    workshops.mutations.setServiceLeadTime(stateClone, 1);
    expect(stateClone.serviceLeadTime).toEqual(1);
  });
  it('updateAvailableWorkshops()', () => {
    workshops.mutations.updateAvailableWorkshops(stateClone, ['foo', 'bar']);
    expect(stateClone.availableWorkshops).toEqual(['foo', 'bar']);
  });
  it('updateDisabledWorkshops()', () => {
    workshops.mutations.updateDisabledWorkshops(stateClone, ['foo', 'bar']);
    expect(stateClone.disabledWorkshops).toEqual(['foo', 'bar']);
  });
});

describe('Store - Workshops [Actions]', () => {
  beforeEach(() => {
    stateClone = cloneDeep(state);
    commit = jest.fn();
  });

  afterEach(() => {
    mockAxios.reset();
  });

  it('getWorkshops() - Success', () => {
    const spy = jest.spyOn(axios, 'get');
    const mockResponse = [
      {
        AvailableHours: 0,
      },
      {
        AvailableHours: 11,
      },
    ];
    mockAxios.onGet().reply(200, mockResponse);
    return workshops.actions.getWorkshops({ commit, state: stateClone, rootState })
      .then(() => {
        expect(spy).toBeCalledWith(expect.stringMatching(/Workshops\/7\/Capacity\?dateTo=\d{4}-\d{2}-\d{2}/));
        expect(commit).toHaveBeenCalledTimes(3);
      });
  });

  it('getWorkshops() - Failure', () => {
    const spy = jest.spyOn(axios, 'get');
    const mockResponse = 'Error foo';
    mockAxios.onGet().reply(400, mockResponse);
    return workshops.actions.getWorkshops({ commit, state: stateClone, rootState })
      .catch(() => {
        expect(spy).toBeCalledWith(expect.stringMatching(/Workshops\/7\/Capacity\?dateTo=\d{4}-\d{2}-\d{2}/));
        expect(commit).toHaveBeenCalledTimes(1);
      });
  });

  it('postRepairOrder() - Success', () => {
    const postData = {
      VehicleId: 'foo',
      OwnerProfileId: 'bar',
      BookingDate: '2018-08-16',
      CustomerComment: 'foo: bar\n\nSelected Services: foo\n\nCustomer Comment: N/A',
      Workshop: '7',
      Odometer: 0,
      ServiceReminder: 'Test',
      ContactNo: '',
      ServiceAdvisor: 'OSB',
      TimeIn: dateObj,
      TimeOut: dateObj,
      DateOut: dateObj,
      Jobs: [
        {
          OperationCode: '',
          OperationDescription: 'testing 1',
          StandardHour: 0.5,
          LabourAmt: 85.01,
          PartAmt: 0,
          SundryAmt: 0,
          TotalAmt: 85.01,
        },
      ],
    };
    const spy = jest.spyOn(axios, 'post');
    const mockResponse = { data: 'foo' };
    mockAxios.onPost().reply(200, mockResponse);
    return workshops.actions.postRepairOrder({ commit, state: stateClone, rootState })
      .then(() => {
        expect(spy).toBeCalledWith('RepairOrders', postData);
        flushPromises()
          .then(() => {
            expect(commit).toHaveBeenCalledTimes(4);
          });
      });
  });

  it('postRepairOrder() - Failure', () => {
    const postData = {
      VehicleId: 'foo',
      OwnerProfileId: 'bar',
      BookingDate: '2018-08-16',
      CustomerComment: 'foo: bar\n\nSelected Services: foo\n\nCustomer Comment: N/A',
      Workshop: '7',
      Odometer: 0,
      ServiceReminder: 'Test',
      ContactNo: '',
      ServiceAdvisor: 'OSB',
      TimeIn: dateObj,
      TimeOut: dateObj,
      DateOut: dateObj,
      Jobs: [
        {
          OperationCode: '',
          OperationDescription: 'testing 1',
          StandardHour: 0.5,
          LabourAmt: 85.01,
          PartAmt: 0,
          SundryAmt: 0,
          TotalAmt: 85.01,
        },
      ],
    };
    const spy = jest.spyOn(axios, 'post');
    const mockResponse = 'Error foo';
    mockAxios.onPost().reply(400, mockResponse);
    return workshops.actions.postRepairOrder({ commit, state: stateClone, rootState })
      .catch(() => {
        expect(spy).toBeCalledWith('RepairOrders', postData);
        flushPromises()
          .then(() => {
            expect(commit).toHaveBeenCalledTimes(2);
          });
      });
  });
});
