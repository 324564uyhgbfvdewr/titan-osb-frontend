import app from '@/store/modules/app';
import { cloneDeep } from 'lodash';

let stateClone;
const state = {
  isLoading: false,
  isComplete: false,
  steps: [
    {
      name: 'VehicleSearch',
      title: 'Find my vehicle.',
      description: 'Search for your vehicle by VIN Number or Registration Number.',
      component: 'VehicleContainer',
      active: true,
      complete: false,
      summary: [],
    },
    {
      name: 'ServiceSelect',
      title: 'Service Selection.',
      description: 'Please select the appropriate service items your BMW requires.',
      component: 'ServiceContainer',
      active: false,
      complete: false,
      summary: [],
    },
    {
      name: 'OptionsSelect',
      title: 'Options Selection.',
      description: 'Please book a date and time for your service along with any other required infomation.',
      component: 'OptionsContainer',
      active: false,
      complete: false,
      summary: [],
    },
    {
      name: 'Confirmation',
      title: 'Confirmation.',
      description: 'Please review your service booking details below',
      component: 'ConfirmationContainer',
      active: false,
      complete: false,
      summary: [],
    },
  ],
};

beforeEach(() => {
  stateClone = cloneDeep(state);
});

describe('Store - App [getters]', () => {
  it('currentStep()', () => {
    const step = app.getters.currentStep(stateClone);
    expect(step).toEqual(stateClone.steps[0]);
  });
});

describe('Store - App [mutations]', () => {
  it('completeStep()', () => {
    app.mutations.completeStep(stateClone, {
      stepName: 'VehicleSearch',
      data: [
        'foo',
        'bar',
      ],
    });
    expect(stateClone.steps[0].active).toEqual(false);
    expect(stateClone.steps[0].complete).toEqual(true);
    expect(stateClone.steps[0].summary).toEqual(['foo', 'bar']);
    expect(stateClone.steps[1].active).toEqual(true);
  });

  it('completeStep() on last step', () => {
    app.mutations.completeStep(stateClone, {
      stepName: 'Confirmation',
      data: [
        'foo',
        'bar',
      ],
    });
    expect(stateClone.steps[3].active).toEqual(false);
    expect(stateClone.steps[3].complete).toEqual(true);
    expect(stateClone.steps[3].summary).toEqual(['foo', 'bar']);
  });

  it('revertStep()', () => {
    stateClone.steps.forEach((step, index) => {
      /* eslint-disable no-param-reassign */
      if (index === stateClone.steps.length - 1) return;
      step.active = false;
      step.complete = true;
      /* eslint-enable no-param-reassign */
    });

    stateClone.steps[stateClone.steps.length - 1].active = true;
    stateClone.steps[stateClone.steps.length - 1].complete = false;

    app.mutations.revertStep(stateClone, 'OptionsSelect');
    const optionsIndex = stateClone.steps.findIndex(step => step.name === 'OptionsSelect');
    stateClone.steps.forEach((step, index) => {
      let active = false;
      let complete = false;
      if (optionsIndex === index) active = true;
      if (optionsIndex > index) complete = true;
      expect(step.active).toEqual(active);
      expect(step.complete).toEqual(complete);
    });
  });

  it('previousStep()', () => {
    stateClone.steps.forEach((step, index) => {
      if (index > 2) return;
      /* eslint-disable no-param-reassign */
      step.active = false;
      step.complete = true;
      /* eslint-enable no-param-reassign */
    });

    app.mutations.previousStep(stateClone, 'OptionsSelect');
    expect(stateClone.steps[1].active).toEqual(true);
    expect(stateClone.steps[1].complete).toEqual(false);
    expect(stateClone.steps[1].summary).toEqual([]);
    expect(stateClone.steps[2].active).toEqual(false);
    expect(stateClone.steps[2].complete).toEqual(false);
    expect(stateClone.steps[2].summary).toEqual([]);
  });

  it('setLoadingState', () => {
    app.mutations.setLoadingState(stateClone, true);
    expect(stateClone.isLoading).toEqual(true);
    app.mutations.setLoadingState(stateClone, false);
    expect(stateClone.isLoading).toEqual(false);
  });

  it('completeBooking', () => {
    app.mutations.completeBooking(stateClone);
    expect(stateClone.isComplete).toEqual(true);
  });
});
