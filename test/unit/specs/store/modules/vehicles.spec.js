import axios from '@/api';
import MockAdapter from 'axios-mock-adapter';
import { cloneDeep } from 'lodash';
import vehicles from '@/store/modules/vehicles';

let stateClone;
const mockAxios = new MockAdapter(axios);

describe('Store - Vehicles [actions]', () => {
  const state = {
    allVehicles: [],
  };
  const commit = jest.fn();

  beforeEach(() => {
    stateClone = cloneDeep(state);
  });

  afterEach(() => {
    mockAxios.reset();
  });

  it('getVehicles() - Success no vehicles', () => {
    const spy = jest.spyOn(axios, 'get');
    const mockResponse = { vehicles: [], token: 'foobarToken' };
    mockAxios.onGet().reply(200, mockResponse);

    return vehicles.actions.getVehicles({ commit, state: stateClone }, { foo: 'bar' })
      .then(() => {
        expect(spy).toBeCalledWith('Vehicles?foo=bar');
        expect(stateClone.allVehicles).toEqual([]);
        expect(axios.defaults.headers.common.Authorization).not.toBeDefined();
      });
  });

  it('getVehicles() - Success with vehicles', () => {
    const spy = jest.spyOn(axios, 'get');
    const mockResponse = { vehicles: ['foo', 'bar'], token: 'foobarToken' };
    mockAxios.onGet().reply(200, mockResponse);

    return vehicles.actions.getVehicles({ commit, state: stateClone }, { foo: 'bar' })
      .then(() => {
        expect(spy).toBeCalledWith('Vehicles?foo=bar');
        expect(stateClone.allVehicles).toEqual(mockResponse.vehicles);
        expect(axios.defaults.headers.common.Authorization).toEqual('Bearer foobarToken');
      });
  });

  it('getVehicles() - Failure', () => {
    const mockResponse = { error: 'error foo' };
    mockAxios.onGet().reply(400, mockResponse);

    return vehicles.actions.getVehicles({ commit, state: stateClone }, { foo: 'bar' })
      .catch((res) => {
        expect(res.response.data).toEqual(mockResponse);
      });
  });
});

describe('Store - Vehicles [mutations]', () => {
  it('clearVehicles()', () => {
    stateClone.allVehicles = [234, 24, 23, 423, 42, 34, 23, 4234, 23, 42, 34, 2, 34, 234];
    vehicles.mutations.clearVehicles(stateClone);
    expect(stateClone.allVehicles).toEqual([]);
  });
});
