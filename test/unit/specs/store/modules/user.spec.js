import user from '@/store/modules/user';
import { cloneDeep } from 'lodash';

const state = {
  profile: {
    Title: {
      title: 'Title',
      validation: 'alpha',
      value: '',
    },
    FirstName: {
      title: 'First Name',
      validation: 'alpha',
      value: '',
    },
    LastName: {
      title: 'Last Name',
      validation: 'alpha',
      value: '',
    },
    Email: {
      title: 'Email',
      validation: 'email',
      value: '',
    },
    Telephone: {
      title: 'Telephone',
      validation: 'numeric',
      value: '',
    },
    HouseNumber: {
      title: 'House Number/Name',
      validation: 'alphanumeric',
      value: '',
    },
    Street: {
      title: 'Street',
      validation: 'alphanumeric',
      value: '',
    },
    Town: {
      title: 'Town',
      validation: 'alpha',
      value: '',
    },
    Postcode: {
      title: 'Postcode',
      validation: 'alphanumeric',
      value: '',
    },
  },
  regex: {
    alphanumeric: new RegExp(/[a-zA-Z0-9 ]+/),
    alpha: new RegExp(/[a-zA-Z ]+/),
    numeric: new RegExp(/\d+/),
    email: new RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/),
  },
};

let stateClone;

beforeEach(() => {
  stateClone = cloneDeep(state);
});

describe('Store - User [mutations]', () => {
  it('updateProfile', () => {
    const profileData = {
      Title: 'foo',
      FirstName: 'foo',
      LastName: 'foo',
      Email: 'foo',
      Telephone: 'foo',
      HouseNumber: 'foo',
      Street: 'foo',
      Town: 'foo',
      Postcode: 'foo',
    };
    user.mutations.updateProfile(stateClone, profileData);
    Object.keys(stateClone.profile).forEach((field) => {
      expect(profileData[field]).toEqual(stateClone.profile[field].value);
    });
  });
});

describe('Store - User [getters]', () => {
  it('profileErrors()', () => {
    const profile = {
      error: {
        Title: 123,
        FirstName: 123123,
        LastName: 1231231,
        Email: 124324,
        Telephone: 'foo',
        HouseNumber: '#',
        Street: '#',
        Town: '#',
        Postcode: '#',
      },
      clean: {
        Title: 'foo',
        FirstName: 'bar',
        LastName: 'baz',
        Email: 'foo@bar.baz',
        Telephone: '12345',
        HouseNumber: '123',
        Street: 'foo',
        Town: 'bar',
        Postcode: 'baz1',
      },
    };
    user.mutations.updateProfile(stateClone, profile.error);
    const errorResultsTrue = user.getters.profileErrors(stateClone);

    expect(errorResultsTrue).toEqual([
      'Title',
      'First Name',
      'Last Name',
      'Email',
      'Telephone',
      'House Number/Name',
      'Street',
      'Town',
      'Postcode',
    ]);

    user.mutations.updateProfile(stateClone, profile.clean);
    const errorResultsFalse = user.getters.profileErrors(stateClone);
    expect(errorResultsFalse.length).toBe(0);
  });
});
