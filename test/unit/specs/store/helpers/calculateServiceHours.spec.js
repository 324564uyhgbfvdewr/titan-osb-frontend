import calculateServiceHours from '@/store/helpers/calculateServiceHours';
import serviceOptions from '../../../mocks/serviceOptions';

describe('calculateServiceHours()', () => {
  const configurations = [
    {
      selected: ['Service', 'Oil & Filter Change'],
      expected: 5,
    },
    {
      selected: ['Brake Fluid Change'],
      expected: 0.5,
    },
    {
      selected: [],
      expected: 0,
    },
  ];
  it('Calculates the total number of hours needed', () => {
    configurations.forEach(({ selected, expected }) => {
      const hours = calculateServiceHours(serviceOptions, selected);
      expect(hours).toEqual(expected);
    });
  });
});
