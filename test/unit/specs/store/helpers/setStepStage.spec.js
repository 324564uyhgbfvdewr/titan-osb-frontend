import setStepStage from '@/store/helpers/setStepStage';

const state = {
  steps: [
    {
      active: false,
      complete: false,
      summary: [],
    },
    {
      active: false,
      complete: false,
      summary: [],
    },
    {
      active: false,
      complete: false,
      summary: [],
    },
    {
      active: false,
      complete: false,
      summary: [],
    },
  ],
};

describe('setStepStage()', () => {
  it('Updates the state', () => {
    const positions = [
      'active',
      'complete',
      'complete',
      '',
    ];

    state.steps.forEach((step, index) => {
      setStepStage(state, index, positions[index]);
      expect(state.steps[index].active).toEqual(positions[index] === 'active');
      expect(state.steps[index].complete).toEqual(positions[index] === 'complete');
      expect(state.steps[index].summary).toEqual([]);
    });
  });
});
