import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailsForm from '@/components/details/DetailsForm';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailsForm.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('updates the store when the form is modified', () => {
    const profileMock = jest.fn();
    const wrapper = shallowMount(DetailsForm, {
      localVue,
      store,
      methods: {
        updateProfile: profileMock,
      },
    });
    wrapper.setData({
      profile: {
        Title: 'foo',
      },
    });
    expect(profileMock).toHaveBeenCalledWith(wrapper.vm.profile);
  });
});
