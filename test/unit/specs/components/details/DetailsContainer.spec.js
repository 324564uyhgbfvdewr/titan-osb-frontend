import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailsContainer from '@/components/details/DetailsContainer';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailsContainer.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('checkProfileData() - Incomplete form', () => {
    const wrapper = shallowMount(DetailsContainer, {
      localVue,
      store,
      computed: {
        profile() {
          return {
            Title: '',
            Name: 'foobar',
          };
        },
      },
    });
    wrapper.vm.checkProfileData();

    expect(wrapper.vm.errors).toEqual([
      'Please complete all form fields marked with a *',
    ]);
  });

  it('checkProfileData() - Invalid form', () => {
    const wrapper = shallowMount(DetailsContainer, {
      localVue,
      store,
      computed: {
        formFilled() {
          return true;
        },
        profileErrors() {
          return ['foo', 'bar'];
        },
      },
    });
    wrapper.vm.checkProfileData();

    expect(wrapper.vm.errors).toEqual([
      'Please enter a valid foo',
      'Please enter a valid bar',
    ]);
  });

  it('checkProfileData() - Success', () => {
    const completeMock = jest.fn();
    const wrapper = shallowMount(DetailsContainer, {
      localVue,
      store,
      computed: {
        profile() {
          return {
            Title: {
              value: 'Mr',
            },
            FirstName: {
              value: 'Foo',
            },
            LastName: {
              value: 'Bar',
            },
            HouseNumber: {
              value: '123',
            },
            Street: {
              value: 'Foo Street',
            },
            Town: {
              value: 'Bar Town',
            },
            Postcode: {
              value: 'F00 B4R',
            },
          };
        },
        formFilled() {
          return true;
        },
        profileErrors() {
          return [];
        },
      },
      methods: {
        completeStep: completeMock,
      },
    });
    wrapper.vm.checkProfileData();

    expect(wrapper.vm.errors).toEqual([]);
    expect(completeMock).toBeCalledWith({
      stepName: 'PersonalDetails',
      data: [
        'Your Details',
        'Mr Foo Bar - 123 Foo Street, Bar Town, F00 B4R',
      ],
    });
  });
});
