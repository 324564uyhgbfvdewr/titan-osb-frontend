import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import CompleteContainer from '@/components/complete/CompleteContainer';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ConfirmationContainer.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('reloadWindow()', () => {
    Object.defineProperty(window.location, 'reload', {
      configurable: true,
      // writable: true,
    });
    window.location.reload = jest.fn();

    const wrapper = shallowMount(CompleteContainer, { localVue, store });
    wrapper.vm.reloadWindow();
    expect(window.location.reload).toHaveBeenCalled();
  });
});
