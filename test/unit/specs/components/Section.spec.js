import { shallowMount, createLocalVue } from '@vue/test-utils';
import { cloneDeep } from 'lodash';
import Vuex from 'vuex';
import Section from '@/components/Section';

const localVue = createLocalVue();
localVue.use(Vuex);

jest.useFakeTimers();

describe('Section.vue', () => {
  let wrapper;
  let store;
  let mutations;

  const step = {
    name: 'SectionFoobarName',
    title: 'Section Foobar',
    description: 'Description Foobar',
    component: 'DummyComponent',
    active: true,
    complete: false,
    summary: [
      'foo',
      'bar',
    ],
  };

  beforeEach(() => {
    mutations = {
      revertStep: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        app: {
          namespaced: true,
          mutations,
        },
      },
    });
    wrapper = shallowMount(Section, {
      propsData: {
        step,
        stepKey: step.title,
      },
      store,
      localVue,
    });
  });

  it('Renders correctly when active', () => {
    const title = wrapper.find('.section--title').html();
    const description = wrapper.find('.section--description').html();
    expect(title).toContain('Section Foobar');
    expect(description).toContain('Description Foobar');
    expect(wrapper.html()).toContain('section_active');
  });

  it('Renders correctly when complete', () => {
    const stepClone = cloneDeep(step);
    stepClone.active = false;
    stepClone.complete = true;
    wrapper.setProps({
      step: stepClone,
    });
    expect(wrapper.html()).toContain('section--tick');
    expect(wrapper.html()).toContain('section--title_summary');
    expect(wrapper.html()).not.toContain('Section Foobar');
    expect(wrapper.html()).toContain('section_complete');
  });

  it('Reverts the step when header is clicked and in complete state', () => {
    const stepClone = cloneDeep(step);
    stepClone.active = false;
    stepClone.complete = true;
    wrapper.setProps({
      step: stepClone,
    });

    const header = wrapper.find('.section--header');
    header.trigger('click');
    expect(mutations.revertStep).toBeCalled();
  });

  it('Triggers scrollToSection() if the section goes from inactive -> active', () => {
    const stepClone = cloneDeep(step);
    wrapper.vm.scrollToSection = jest.fn();

    stepClone.active = true;
    wrapper.setProps({
      step: stepClone,
    });

    stepClone.active = false;
    wrapper.setProps({
      step: stepClone,
    });
    jest.runAllTimers();
    expect(wrapper.vm.scrollToSection).not.toBeCalled();

    stepClone.active = true;
    wrapper.setProps({
      step: stepClone,
    });
    jest.runAllTimers();
    expect(wrapper.vm.scrollToSection).toBeCalled();
  });

  it('scrollToSection()', () => {
    window.scroll = jest.fn();
    wrapper.vm.scrollToSection();
    expect(window.scroll).toBeCalledWith({
      top: expect.any(Number),
      behavior: 'smooth',
    });
  });
});
