import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import OptionsContainer from '@/components/options/OptionsContainer';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('checkStepState()', () => {
  const store = new Vuex.Store(storeMock);

  it('Doesn\'t complete the step if a date isn\'t selected', () => {
    const completeMock = jest.fn();
    const wrapper = shallowMount(OptionsContainer, {
      localVue,
      store,
      methods: {
        completeStep: completeMock,
      },
      computed: {
        servicePeriod: () => 'AM',
        serviceDate: () => false,
      },
    });
    wrapper.vm.checkStepState();
    expect(completeMock).not.toBeCalled();
  });

  it('Completes the step if a date is selected', () => {
    const completeMock = jest.fn();
    const wrapper = shallowMount(OptionsContainer, {
      localVue,
      store,
      methods: {
        completeStep: completeMock,
      },
      computed: {
        serviceDate: () => '2018-07-26',
      },
    });
    wrapper.vm.checkStepState();
    expect(completeMock).toBeCalled();
  });
});
