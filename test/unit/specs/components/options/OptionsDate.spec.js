import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import OptionsDate from '@/components/options/OptionsDate';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ServiceContainer.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('disabledDates() with no disabledWorkshops', () => {
    const wrapper = shallowMount(OptionsDate, {
      localVue,
      store,
      computed: {
        availableWorkshops: jest.fn().mockReturnValue([]),
        disabledWorkshops: jest.fn().mockReturnValue([]),
      },
    });
    expect(wrapper.vm.disabledDates).toEqual({});
  });

  it('disabledDates() with disabledWorkshops', () => {
    const wrapper = shallowMount(OptionsDate, {
      localVue,
      store,
      computed: {
        availableWorkshops: jest.fn().mockReturnValue([
          {
            WorkDate: '2018-01-01',
          },
        ]),
        disabledWorkshops: jest.fn().mockReturnValue([
          {
            WorkDate: '2018-01-01',
          },
          {
            WorkDate: '2018-01-01',
          },
          {
            WorkDate: '2018-01-01',
          },
        ]),
      },
    });
    expect(wrapper.vm.disabledDates).toEqual(expect.objectContaining({
      dates: expect.any(Array),
      from: expect.any(Object),
      to: expect.any(Object),
    }));
  });
});
