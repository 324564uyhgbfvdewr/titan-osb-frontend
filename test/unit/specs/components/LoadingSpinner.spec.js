import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import LoadingSpinner from '@/components/LoadingSpinner';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('LoadingSpinner.vue', () => {
  const state = { isLoading: true };
  const store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        state,
      },
    },
  });

  it('Shows a spinner when the app is loading', () => {
    const wrapper = shallowMount(LoadingSpinner, { store, localVue });
    expect(wrapper.vm.isLoading).toBe(true);
    expect(wrapper.html()).toContain('loading--spinner');
  });

  it('Hides the loading spinner when the app isn\'t loading', () => {
    state.isLoading = false;
    const wrapper = shallowMount(LoadingSpinner, { store, localVue });
    expect(wrapper.vm.isLoading).toBe(false);
    expect(wrapper.isEmpty()).toBe(true);
  });
});
