import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import VehicleSearch from '@/components/vehicle/VehicleSearch';
import regoMocks from '../../../mocks/rego';
import vinMocks from '../../../mocks/vin';
import flushPromises from '../../../mocks/flushPromises';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('VehicleSearch.vue', () => {
  let store;
  let actions;
  let mutations;

  beforeEach(() => {
    actions = {
      getVehicles: jest.fn(),
    };

    mutations = {
      clearVehicles: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        app: {
          namespaced: true,
          state: {
            isLoading: false,
          },
        },
        vehicles: {
          namespaced: true,
          state: {
            allVehicles: [],
          },
          actions,
          mutations,
        },
      },
    });
  });

  it('isValidInput() - returns true or false based on input validity', () => {
    const wrapper = shallowMount(VehicleSearch, { store, localVue });

    wrapper.setData({ selectedSearch: 'Registration' });
    regoMocks.forEach((test) => {
      wrapper.setData({ inputData: test.value });
      expect(wrapper.vm.isValidInput()).toBe(test.valid);
    });

    wrapper.setData({ selectedSearch: 'VIN' });
    vinMocks.forEach((test) => {
      wrapper.setData({ inputData: test.value });
      expect(wrapper.vm.isValidInput()).toBe(test.valid);
    });
  });

  it('findVehicle() - sets error if input is not valid', () => {
    const wrapper = shallowMount(VehicleSearch, { store, localVue });

    wrapper.setData({
      selectedSearch: 'VIN',
      inputData: 'foobar@',
    });
    wrapper.vm.findVehicle();
    expect(wrapper.vm.errorMessage).toEqual('Please enter a valid VIN');
  });

  it('findVehicle() - hits API and returns vehicles if input is valid and matched', (done) => {
    actions.getVehicles.mockResolvedValue(['foo', 'bar']);
    const wrapper = shallowMount(VehicleSearch, { store, localVue });
    wrapper.setData({
      selectedSearch: 'Registration',
      inputData: '12345',
    });
    wrapper.vm.findVehicle();
    Vue.nextTick(() => {
      expect(wrapper.vm.errorMessage).toEqual('');
      done();
    });
  });

  it('findVehicle() - hits API and returns an error if input is valid and not matched', (done) => {
    actions.getVehicles.mockResolvedValue([]);
    const wrapper = shallowMount(VehicleSearch, { store, localVue });
    wrapper.setData({
      selectedSearch: 'VIN',
      inputData: '12345678912345678',
    });
    wrapper.vm.findVehicle();
    Vue.nextTick(() => {
      expect(wrapper.vm.errorMessage).toEqual('No vehicles found, please try a different VIN');
      done();
    });
  });

  it('findVehicle() - hits API and returns an error if there is an API error', (done) => {
    actions.getVehicles.mockRejectedValue('Error foo');
    const wrapper = shallowMount(VehicleSearch, { store, localVue });
    wrapper.setData({
      selectedSearch: 'Registration',
      inputData: '12345',
    });
    wrapper.vm.findVehicle();
    flushPromises()
      .then(() => {
        expect(actions.getVehicles).toHaveBeenCalled();
        expect(wrapper.vm.errorMessage).toEqual('No vehicles found, please try a different Registration');
        done();
      });
  });
});
