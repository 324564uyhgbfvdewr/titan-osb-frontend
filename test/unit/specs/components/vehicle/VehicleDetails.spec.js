import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VehicleDetails from '@/components/vehicle/VehicleDetails';
import vehicles from '../../../mocks/vehicles';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('VehicleDetails.vue', () => {
  const store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        state: {
          isLoading: true,
        },
        mutations: {
          completeStep: jest.fn(),
        },
      },
      vehicles: {
        namespaced: true,
        state: {
          allVehicles: [],
        },
        mutations: {
          clearVehicles: jest.fn(),
        },
      },
    },
  });

  it('Renders the vehicle details if they are available', () => {
    const wrapper = shallowMount(VehicleDetails, {
      store,
      propsData: {
        vehicle: vehicles[0],
      },
    });
    expect(wrapper.findAll('.vehicle-details--item').length).toEqual(4);
  });

  it('Renders the vehicle name if data is available', () => {
    const wrapper = shallowMount(VehicleDetails, {
      store,
      propsData: {
        vehicle: {},
      },
    });
    expect(wrapper.vm.vehicleName).toEqual('');

    wrapper.setProps({ vehicle: vehicles[0] });
    expect(wrapper.vm.vehicleName).toEqual('TOYOTA YARIS');
  });

  it('Creates a vehicle summary', () => {
    const wrapper = shallowMount(VehicleDetails, {
      store,
      propsData: {
        vehicle: vehicles[0],
      },
    });
    expect(wrapper.vm.stepSummary).toEqual([
      wrapper.vm.vehicleName,
      `Registration: ${vehicles[0].RegoNo}`,
    ]);
  });
});
