import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VehicleContainer from '@/components/vehicle/VehicleContainer';
import vehicles from '../../../mocks/vehicles';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('VehicleContainer.vue', () => {
  const state = {
    allVehicles: vehicles,
  };
  const store = new Vuex.Store({
    modules: {
      vehicles: {
        namespaced: true,
        state,
      },
    },
  });

  it('Returns the first vehicle in a list', () => {
    const wrapper = shallowMount(VehicleContainer, {
      store,
      localVue,
    });

    expect(wrapper.vm.firstVehicle).toEqual(vehicles[0]);
  });

  it('Returns any empty object if no vehicles are found', () => {
    state.allVehicles = [];
    const wrapper = shallowMount(VehicleContainer, {
      store,
      localVue,
    });
    expect(wrapper.vm.firstVehicle).toEqual({});
  });
});
