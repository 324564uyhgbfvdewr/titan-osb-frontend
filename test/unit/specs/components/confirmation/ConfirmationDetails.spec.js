import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ConfirmationDetails from '@/components/confirmation/ConfirmationDetails';
import storeMock from '../../../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ConfirmationDetails.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('vehicleName [computed]', () => {
    const wrapper = shallowMount(ConfirmationDetails, {
      localVue,
      store,
      computed: {
        allVehicles() {
          return [
            {
              Make: 'Foo',
              Model: 'Bar',
            },
          ];
        },
      },
    });
    expect(wrapper.vm.vehicleName).toBe('Foo Bar');
  });

  it('confirmationData [computed] - no data', () => {
    const wrapper = shallowMount(ConfirmationDetails, { localVue, store });
    expect(wrapper.vm.confirmationData.length).toBe(0);
  });

  it('confirmationData [computed] - expected data (no comments)', () => {
    const wrapper = shallowMount(ConfirmationDetails, {
      localVue,
      store,
      computed: {
        allVehicles: () => [{ RegoNo: 'F00B4R' }],
        selectedServices: () => ['ServiceFoo', 'ServiceBar'],
        serviceDate: () => '2018-08-01',
        serviceComments: () => '',
      },
    });
    expect(wrapper.vm.confirmationData).toEqual([
      {
        title: 'Registration Number',
        value: 'F00B4R',
      },
      {
        title: 'Service Date',
        value: 'Wednesday 1st of August',
      },
      {
        title: 'Service Time',
        value: 'Your dealer will contact you to arrange drop-off time',
      },
      {
        title: 'Services Selected',
        value: 'ServiceFoo, ServiceBar',
      },
      {
        title: 'Notes',
        value: 'N/A',
      },
    ]);
  });

  it('confirmationData [computed] - expected data (with comments)', () => {
    const wrapper = shallowMount(ConfirmationDetails, {
      localVue,
      store,
      computed: {
        allVehicles: () => [{ RegoNo: 'F00B4R' }],
        selectedServices: () => ['ServiceFoo', 'ServiceBar'],
        serviceDate: () => '2018-08-01',
        serviceComments: () => 'Comment Foo',
      },
    });
    expect(wrapper.vm.confirmationData).toEqual([
      {
        title: 'Registration Number',
        value: 'F00B4R',
      },
      {
        title: 'Service Date',
        value: 'Wednesday 1st of August',
      },
      {
        title: 'Service Time',
        value: 'Your dealer will contact you to arrange drop-off time',
      },
      {
        title: 'Services Selected',
        value: 'ServiceFoo, ServiceBar',
      },
      {
        title: 'Notes',
        value: 'Comment Foo',
      },
    ]);
  });
});
