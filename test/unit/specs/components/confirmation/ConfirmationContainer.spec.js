import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ConfirmationContainer from '@/components/confirmation/ConfirmationContainer';
import storeMock from '../../../mocks/store';
import flushPromises from '../../../mocks/flushPromises';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ConfirmationContainer.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('completeBooking() - Success', () => {
    const wrapper = shallowMount(ConfirmationContainer, {
      localVue,
      store,
      methods: {
        postRepairOrder: jest.fn().mockResolvedValue(),
      },
    });
    wrapper.vm.completeBooking();
    flushPromises()
      .then(() => {
        expect(wrapper.vm.error).toBe(false);
      });
  });

  it('completeBooking() - Failure', () => {
    const wrapper = shallowMount(ConfirmationContainer, {
      localVue,
      store,
      methods: {
        postRepairOrder: jest.fn().mockRejectedValue(),
      },
    });
    wrapper.vm.completeBooking();
    flushPromises()
      .then(() => {
        expect(wrapper.vm.error).toBe(true);
      });
  });
});
