import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ServiceOption from '@/components/service/ServiceOption';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ServiceOption.vue', () => {
  const state = {
    selectedServices: [],
  };
  const store = new Vuex.Store({
    modules: {
      services: {
        namespaced: true,
        state,
        mutations: {
          toggleService: jest.fn(),
        },
      },
    },
  });

  it('Renders an option with an icon and information', () => {
    const wrapper = shallowMount(ServiceOption, {
      store,
      localVue,
      propsData: {
        option: {
          name: 'Foobar',
          icon: 'BrakePadIcon',
        },
      },
    });

    expect(wrapper.find('.service-option').classes()).toContain('color--light-grey');
  });


  it('Toggles the option\'s active state once clicked/or hit return', () => {
    const wrapper = shallowMount(ServiceOption, {
      store,
      localVue,
      propsData: {
        option: {
          name: 'Foobar',
          icon: 'BrakePadIcon',
        },
      },
    });

    const checkbox = wrapper.find('.service-option--checkbox');

    checkbox.trigger('click');
    expect(wrapper.find('.service-option').classes()).toContain('color--primary');

    checkbox.trigger('focus');
    checkbox.trigger('keyup.enter');
    expect(wrapper.find('.service-option').classes()).toContain('color--light-grey');
  });
});
