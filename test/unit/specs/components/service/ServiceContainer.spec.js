import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ServiceContainer from '@/components/service/ServiceContainer';
import storeMock from '../../../mocks/store';
import flushPromises from '../../../mocks/flushPromises';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ServiceContainer.vue', () => {
  const store = new Vuex.Store(storeMock);

  it('splitNewLines()', () => {
    const strings = [
      {
        value: 'this\nis',
        lines: 2,
      },
      {
        value: 'this\nis\nthree',
        lines: 3,
      },
      {
        value: 'this\nis\nfour\nlines',
        lines: 4,
      },
    ];
    const wrapper = shallowMount(ServiceContainer, { localVue, store });

    strings.forEach((string) => {
      const stringArr = wrapper.vm.splitNewLines(string.value);
      expect(stringArr.length).toBe(string.lines);
    });
  });

  it('checkStepState() - no service options selected', () => {
    const wrapper = shallowMount(ServiceContainer, { localVue, store });
    wrapper.vm.checkStepState();

    expect(wrapper.vm.error).toBe('Please select at least one service item.');
  });

  it('checkStepState() - service options selected', () => {
    const completeMock = jest.fn();
    const wrapper = shallowMount(ServiceContainer, {
      localVue,
      store,
      computed: {
        selectedServices: () => ['foo', 'bar'],
      },
      methods: {
        completeStep: completeMock,
      },
    });
    wrapper.vm.checkStepState();
    flushPromises()
      .then(() => {
        expect(completeMock).toBeCalledWith({
          stepName: 'ServiceSelect',
          data: [
            'Services Selected',
            'foo, bar',
          ],
        });
      });
  });

  it('checkStepState() - request error', () => {
    const completeMock = jest.fn();
    const wrapper = shallowMount(ServiceContainer, {
      localVue,
      store,
      computed: {
        selectedServices: () => ['foo', 'bar'],
      },
      methods: {
        completeStep: completeMock,
      },
    });
    wrapper.vm.getWorkshops = jest.fn().mockRejectedValue();
    wrapper.vm.checkStepState();
    flushPromises()
      .then(() => {
        expect(wrapper.vm.error).toBe('An error occurred. Please try again.');
      });
  });
});
