import dateFuture from '../../../../src/functions/dateFuture';

const milisecondsInDays = days => Math.floor(days / 24 / 60 / 60 / 1000);

describe(('dateFuture()'), () => {
  it('returns a date object X days into the future', () => {
    const daysArr = [
      30,
      60,
      90,
      120,
    ];

    daysArr.forEach((daysInFuture) => {
      const dateObject = new Date();
      const dateNow = +dateObject;
      const dateInFuture = +dateFuture(daysInFuture, dateObject);

      const difference = milisecondsInDays(dateInFuture - dateNow);

      expect(difference).toEqual(daysInFuture);
    });
  });

  it('returns a date object X days into the future if no date is passed', () => {
    const dateObject = new Date();
    const dateNow = +dateObject;
    const dateInFuture = +dateFuture(2);

    const difference = milisecondsInDays(dateInFuture - dateNow);

    expect(difference).toBeGreaterThan(1);
  });
});
