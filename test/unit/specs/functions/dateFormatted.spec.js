import dateFormatted from '../../../../src/functions/dateFormatted';

describe(('dateFormatted()'), () => {
  it('returns a date in "dd/MM/yyyy" format', () => {
    const dates = [
      ['2018', '01', '28'],
      ['2018', '10', '01'],
      ['2015', '06', '15'],
    ];
    dates.forEach((date) => {
      expect(dateFormatted('dd/MM/yyyy', new Date(...date))).toMatch(/\d{2}\/\d{2}\/\d{4}/);
    });
  });

  it('returns a date in "D dsu of MMM" format', () => {
    const dates = [
      {
        date: ['2018', '06', '01'],
        string: 'Sunday 1st of July',
      },
      {
        date: ['2018', '06', '02'],
        string: 'Monday 2nd of July',
      },
      {
        date: ['2018', '06', '03'],
        string: 'Tuesday 3rd of July',
      },
      {
        date: ['2018', '06', '04'],
        string: 'Wednesday 4th of July',
      },
      {
        date: ['2018', '06', '24'],
        string: 'Tuesday 24th of July',
      },
    ];
    dates.forEach((date) => {
      expect(dateFormatted('D dsu of MMM', new Date(...date.date))).toEqual(date.string);
    });
  });

  it('returns a date in "yyyy-MM-dd" format if none is specified', () => {
    new Array(5).fill(null).forEach(() => {
      expect(dateFormatted()).toMatch(/\d{4}-\d{2}-\d{2}/);
    });
  });
});
