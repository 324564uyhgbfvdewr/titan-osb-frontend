import querystring from '@/functions/querystring';
import queries from '../../mocks/queries';

describe('functions - querystring', () => {
  it('Generates querystrings as "key=value" pairs, joined by "&"', () => {
    queries.forEach((query) => {
      expect(querystring(query.data)).toEqual(query.expected);
    });
  });
});
