import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import App from '@/App';
import storeMock from '../mocks/store';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('App.vue', () => {
  let wrapper;

  beforeEach(() => {
    const store = new Vuex.Store(storeMock);
    wrapper = shallowMount(App, { store, localVue });
  });

  it('Renders correctly', () => {
    expect(wrapper.html()).toContain('Book a Service');
  });
});
