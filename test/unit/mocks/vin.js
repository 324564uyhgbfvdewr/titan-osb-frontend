export default [
  {
    value: '12345678912345678',
    valid: true,
  },
  {
    value: '43567j35678436578',
    valid: true,
  },
  {
    value: '6#435434 5345',
    valid: false,
  },
  {
    value: '1234567891234 67',
    valid: false,
  },
  {
    value: '',
    valid: false,
  },
];
