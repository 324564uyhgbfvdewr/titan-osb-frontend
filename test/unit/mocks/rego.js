export default [
  {
    value: '0123456789',
    valid: false,
  },
  {
    value: '012hsd67',
    valid: true,
  },
  {
    value: '012345678',
    valid: true,
  },
  {
    value: '@~sdfsdf',
    valid: false,
  },
  {
    value: 'abcd1234',
    valid: true,
  },
  {
    value: '',
    valid: false,
  },
];
