export default {
  modules: {
    app: {
      namespaced: true,
      state: {
        isLoading: false,
        isComplete: false,
        currentStep: '',
        steps: [{
          name: 'FooBar',
          title: 'Foo Bar',
          description: 'Description Baz',
          component: 'DummyComponent',
          active: true,
          complete: false,
          summary: [],
        }],
      },
      mutations: {
        completeStep: jest.fn(),
      },
    },

    services: {
      namespaced: true,
      state: {
        serviceOptions: [],
        selectedServices: [],
      },
    },

    workshops: {
      namespaced: true,
      mutations: {
        setWorkshopID: jest.fn(),
        setServiceLeadTime: jest.fn(),
      },
      actions: {
        getWorkshops: jest.fn(),
      },
    },

    vehicles: {
      namespaced: true,
      state: {
        allVehicles: [],
      },
    },

    user: {
      namespaced: true,
      mutations: {
        updateProfile: jest.fn(),
      },
      state: {
        profile: {
          Title: {
            title: 'Title',
            validation: 'alpha',
            value: '',
          },
          FirstName: {
            title: 'First Name',
            validation: 'alpha',
            value: '',
          },
          LastName: {
            title: 'Last Name',
            validation: 'alpha',
            value: '',
          },
          Email: {
            title: 'Email',
            validation: 'email',
            value: '',
          },
          Telephone: {
            title: 'Telephone',
            validation: 'numeric',
            value: '',
          },
          HouseNumber: {
            title: 'House Number/Name',
            validation: 'alphanumeric',
            value: '',
          },
          Street: {
            title: 'Street',
            validation: 'alphanumeric',
            value: '',
          },
          Town: {
            title: 'Town',
            validation: 'alpha',
            value: '',
          },
          Postcode: {
            title: 'Postcode',
            validation: 'alphanumeric',
            value: '',
          },
        },
        regex: {
          alphanumeric: new RegExp(/[a-zA-Z0-9 ]+/),
          alpha: new RegExp(/[a-zA-Z ]+/),
          numeric: new RegExp(/\d+/),
          email: new RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/),
        },
      },
    },
  },
};
