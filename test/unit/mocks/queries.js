export default [
  {
    data: {
      foo: 'bar',
      baz: 'foo',
      bar: 'baz',
    },
    expected: 'foo=bar&baz=foo&bar=baz',
  },
  {
    data: {
      foo: 'bar',
      baz: '',
      bar: 'baz',
    },
    expected: 'foo=bar&bar=baz',
  },
  {
    data: {
      foo: '',
      baz: '',
      bar: '',
    },
    expected: '',
  },
];
