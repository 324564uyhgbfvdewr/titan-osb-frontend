export default [
  {
    name: 'Service',
    icon: 'ServiceIcon',
    time: 4,
    description: [],
  },
  {
    name: 'Oil & Filter Change',
    icon: 'OilFilterIcon',
    time: 1,
    description: [],
  },
  {
    name: 'Brake Fluid Change',
    icon: 'BrakePadIcon',
    time: 0.5,
    description: [],
  },
  {
    name: 'Front Brake Pads and Discs',
    icon: 'BrakePadIcon',
    time: 1.5,
    description: [],
  },
  {
    name: 'Rear Brake Pads and Discs',
    icon: 'BrakePadIcon',
    time: 1.5,
    description: [],
  },
  {
    name: 'Front Brake Pads',
    icon: 'BrakePadIcon',
    time: 0.5,
    description: [],
  },
  {
    name: 'Rear Brake Pads/Shoes',
    icon: 'BrakePadIcon',
    time: 0.5,
    description: [],
  },
  {
    name: 'Replace Front Wiper Blades (Pair)',
    icon: 'WipersPairIcon',
    time: 0.2,
    description: [],
  },
  {
    name: 'Rear Windscreen Wiper',
    icon: 'WiperIcon',
    time: 0.2,
    description: [],
  },
];
